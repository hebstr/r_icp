import streamlit as st
import pandas as pd
import os
# from function import check_password

# if not check_password("secrets_crpv.toml"):
#     st.stop()

data = pd.read_csv("/data/HOME/edjulien/clenbuterol/clenbuterol.csv", dtype=str)

documents = list(data["text"])
documents = [i.replace("#A0A0A0", "#FFFFFF") for i in documents]

SESSION = "clenbuterol"

reponse_path = f"/data/HOME/edjulien/clenbuterol/output_{SESSION}.xlsx"
if os.path.exists(reponse_path):
    ref = pd.read_excel(reponse_path)
else:
    ref = data[["id_entrepot", "id_pat"]].copy()
    ref.loc[:, "Toxidermie"] = ""
    ref.loc[:, "Medicament"] = " "
    ref.loc[:, "Instant_Delayed"] = ""
    ref.loc[:, "Declared"] = ""
    # ref.loc[:, "Time_Elapsed"] = 0
    ref.loc[:, "Document"] = [f"Document {i}" for i in range(len(documents))]

# prendre tout l'espace de l'écran
st.set_page_config(layout="wide")

# Récupérer le dernière index complété
max_index = ref[(ref["Toxidermie"] == "Oui") | (ref["Toxidermie"] == "Non")].index.max()
max_index = 0 if pd.isna(max_index) else max_index

# Initialize session state
if "doc_index" not in st.session_state:
    st.session_state.doc_index = max_index
if "Toxidermie" not in st.session_state:
    st.session_state.Toxidermie = ref["Toxidermie"].iloc[st.session_state.doc_index]
if "medicament" not in st.session_state:
    st.session_state.medicament = ref["Medicament"].iloc[st.session_state.doc_index]
if "instant_delayed" not in st.session_state:
    st.session_state.instant_delayed = ref["Instant_Delayed"].iloc[
        st.session_state.doc_index
    ]
if "declared" not in st.session_state:
    st.session_state.declared = ref["Declared"].iloc[st.session_state.doc_index]


def reset():
    st.session_state.tox = (
        None
        if ref["Toxidermie"].iloc[st.session_state.doc_index] not in ["Oui", "Non"]
        else ref["Toxidermie"].iloc[st.session_state.doc_index]
    )
    st.session_state.doc = ref["Medicament"].iloc[st.session_state.doc_index]
    st.session_state.dec = (
        None
        if ref["Declared"].iloc[st.session_state.doc_index] not in ["Oui", "Non"]
        else ref["Declared"].iloc[st.session_state.doc_index]
    )
    st.session_state.inst = (
        None
        if ref["Instant_Delayed"].iloc[st.session_state.doc_index]
        not in ["Immédiat", "Retardé"]
        else ref["Instant_Delayed"].iloc[st.session_state.doc_index]
    )


def update():
    st.session_state.doc_index = number
    reset()


d_toxi = {"Oui": 0, "Non": 1, " ": None}


def number_change():
    st.session_state.doc_index = st.session_state.num
    reset()


number = st.sidebar.number_input(
    "Numéro document",
    value=st.session_state.doc_index,
    min_value=0,
    max_value=600,
    step=1,
    key="num",
    on_change=number_change,
)


def update_precedent():
    if st.session_state.doc_index > 0:
        st.session_state.doc_index -= 1
    reset()


# Navigation buttons
col1, col2, col3 = st.columns([1, 2, 1])
with col1:
    st.button(
        "Précédent",
        help="Go to the previous document",
        use_container_width=True,
        on_click=update_precedent,
    )


def update_suivant():
    ref.loc[st.session_state.doc_index, "Toxidermie"] = st.session_state.Toxidermie
    ref.loc[st.session_state.doc_index, "Medicament"] = st.session_state.medicament
    ref.loc[st.session_state.doc_index, "Instant_Delayed"] = (
        st.session_state.instant_delayed
    )
    ref.loc[st.session_state.doc_index, "Declared"] = st.session_state.declared
    ref.to_excel(reponse_path, index=False)
    if st.session_state.doc_index < len(documents) - 1:
        st.session_state.doc_index += 1
    reset()


with col3:
    st.button(
        "Suivant",
        help="Go to the next document",
        use_container_width=True,
        on_click=update_suivant,
    )


def update_save():
    ref.loc[st.session_state.doc_index, "Toxidermie"] = st.session_state.Toxidermie
    ref.loc[st.session_state.doc_index, "Medicament"] = st.session_state.medicament
    ref.loc[st.session_state.doc_index, "Instant_Delayed"] = (
        st.session_state.instant_delayed
    )
    ref.loc[st.session_state.doc_index, "Declared"] = st.session_state.declared
    ref.to_excel(reponse_path, index=False)


st.session_state.Toxidermie = ref["Toxidermie"].iloc[st.session_state.doc_index]
st.session_state.medicament = ref["Medicament"].iloc[st.session_state.doc_index]
st.session_state.instant_delayed = ref["Instant_Delayed"].iloc[
    st.session_state.doc_index
]
st.session_state.declared = ref["Declared"].iloc[st.session_state.doc_index]

# st.write(f"Saved : Toxidermie : {st.session_state.Toxidermie} - Doc {st.session_state.medicament} - Méca ")

id_entrepot = ref["id_entrepot"].iloc[st.session_state.doc_index]
id_pat = ref["id_pat"].iloc[st.session_state.doc_index]
# Display the current document
st.write(
    f"Document {st.session_state.doc_index} sur {len(documents)-1} - ID : {id_entrepot} - ID_PAT : {id_pat}"
)

# HTML content with scrolling enabled
doc_content = documents[st.session_state.doc_index]
html_content = f"""
    <html>
    <head>
        <style>
            .scrollable-content {{
                height: 700px; /* Adjust this height as needed */
                overflow-y: scroll;
                border: 1px solid #ddd;
                padding: 10px;
            }}
        </style>
    </head>
    <body>
        <div class="scrollable-content">
            {doc_content}
        </div>
    </body>
    </html>
"""
st.components.v1.html(html_content, height=700)  # Adjust height as needed

# Sidebar for classification inputs
st.sidebar.title(f"Classification - {SESSION}")

# Accepted or Refused (Binary)
toxidermie = st.sidebar.radio(
    "Toxidermie :",
    ("Oui", "Non"),
    index=(
        None
        if st.session_state.Toxidermie not in ["Oui", "Non"]
        else d_toxi[st.session_state.Toxidermie]
    ),
    key="tox",
)
st.session_state.Toxidermie = toxidermie

# Medicament (Text)
medicament = st.sidebar.text_input(
    "Medicament(s) imputable(s)",
    st.session_state.medicament,
    key="doc",
)
st.session_state.medicament = medicament

d_delayed = {"Immédiat": 0, "Retardé": 1, " ": None}
# Instant or Delayed (Binary)
instant_delayed = st.sidebar.radio(
    "Mécanisme probable :",
    ("Immédiat", "Retardé"),
    index=(
        None
        if st.session_state.instant_delayed not in ["Immédiat", "Retardé"]
        else d_delayed[st.session_state.instant_delayed]
    ),
    key="inst",
)
st.session_state.instant_delayed = instant_delayed

# Declared or Not (Binary)
declared = st.sidebar.radio(
    "Incident déclaré :",
    ("Oui", "Non"),
    index=(
        None
        if st.session_state.declared not in ["Oui", "Non"]
        else d_toxi[st.session_state.declared]
    ),
    key="dec",
)
st.session_state.declared = declared

st.sidebar.button(
    "Enregistrer",
    help="Go to the next document",
    use_container_width=True,
    on_click=update_save,
)

# st.json(st.session_state)
