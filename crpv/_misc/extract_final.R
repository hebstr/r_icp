library(tidyverse)
library(glue)
library(rlang)
library(tidytext)
library(htmltools)
library(openxlsx2)

config <- .config

data = ains_2018_01_clean[1:1000, ]
text_input = "text"
sample = NULL
seed = NULL
filter = NULL
id = "id_entrepot"
group = "ipp"
ngrams = 1:4
concepts = .config$concepts$ains
concepts_collapse = FALSE
concepts_intersect = TRUE
starts_with_only = FALSE
exclus_manual = NULL
exclus_auto_except = NULL
regex_replace = NULL
mismatch_data = FALSE
xlsx_save = TRUE
concept_color = "#0099EE"
text_color = "red"
text_background = "#FFFF44"
dir_suffix = sample
filename_suffix = sample

### CONCEPTS -------------------------------------------------------------------

  if (length(concepts) > 1) {

    if (!is_named(concepts)) {

      cli_abort("Chaque concept doit être nommé")

    } else {

      concepts <- as.list(concepts)

    }

  } else if (!is.list(concepts)) {

    if (!is_named(concepts)) {

      concepts <- list("<concept>" = concepts)

    } else {

      concepts <- as.list(concepts)

    }

  }

  if (concepts_collapse) {

    concepts <- concepts |> map(paste, collapse = "|")

  } else {

    concepts <-
    concepts |>
      pluck_depth() |>
      seq() |>
      reduce(~ list_flatten(.), .init = concepts)

  }

  concepts_keys <- names(concepts)

  concepts_names <- concepts |> imap(~ { if (is_named(.x)) names(.x) else .y })

  concepts_root <- concepts_keys |> str_remove("_.+") |> unique()

  if (length(concepts_root) == 1 && concepts_intersect) {

    cli_abort("{.strong Pas d'intersection possible}")

  }

### ----------------------------------------------------------------------------

  if (is.character(data)) data <- get(data)

  data_init <- data

  check_group <- group %in% names(data_init)

  if (!id %in% names(data_init)) {

    id <- "n_id"
    data <- data |> rownames_to_column(id)

  }

  if (!check_group) {

    group <- "n_group"
    data <-
    mutate(data,
           !!group := row_number(),
           .after = all_of(id))

  }

### SAVE PARAMS ----------------------------------------------------------------

  filename <- config$file
  dirname <- glue("{filename}_extract")
  save_dir <- glue("{config$dir}/{dirname}")
  save_files <- dirname

  if (!is.null(dir_suffix)) save_dir <- glue("{save_dir}_{dir_suffix}")

  if (!is.null(filename_suffix)) save_files <- glue("{dirname}_{filename_suffix}")

  if (!file.exists(glue(save_dir))) dir.create(path = glue(save_dir))

  save_extract <- glue("{save_dir}/{save_files}")

### FILTERS --------------------------------------------------------------------

  if (!is.null(sample)) data <- data[sample(nrow(data), sample), ]

  filter <- enexpr(filter)

  if (!is.null(filter)) data <- filter(data, !!filter)

### REPLACE --------------------------------------------------------------------

  data_replace <-
  data[c(id, group, text_input)] |>
  mutate(!!text_input :=
           get(text_input) |>
             str_split("\n") |>
             map_chr(~ . |>
                       str_extract("(?<=>).+(?=</\\w+>)") |>
                       str_replace_all("\\.|'|</?\\w+/?>", " ") |>
                       na.omit() |>
                       paste(collapse = " ") |>
                       str_replace_all("\\s+", " ")) |>
             iconv(from = "UTF-8",
                   to = "ASCII//TRANSLIT"))

### TOKENIZE -------------------------------------------------------------------

  data_ngrams <-
  ngrams |>
    map(~ data_replace |>
          unnest_tokens(output = !!text_input,
                        input = !!text_input,
                        token = "ngrams",
                        n = .) |>
          filter(str_detect(get(text_input), "[:alpha:]")))

### MATCHING -------------------------------------------------------------------

  concepts_regex_end <- if (starts_with_only) "" else "\\S*$"

  concepts_regex <- concepts |> map(~ glue("^({.}){concepts_regex_end}"))

  concepts_df <-
  tibble(concept_key = concepts_keys,
         concept_name = unlist(concepts_names),
         regex = unlist(concepts_regex))

  ngrams_extract <- \(x, n) {

    x |>
      imap(~ data_ngrams[[n]] |>
             mutate(!!text_input := str_extract(get(text_input), .x),
                    concept = .y, .before = text_input) |>
             drop_na())

  }

  data_ngrams_list <-
  ngrams |>
    map(~ concepts_regex |>
          ngrams_extract(.) |>
          list_rbind()) |>
    set_names(ngrams)

  data_ngrams_match <-
  data_ngrams_list |>
    map(~ . |>
          count(concept, pick(text_input),
                name = "match",
                sort = TRUE))

  data_match_init <-
  data_ngrams_list |>
    imap(~ mutate(.x, ngrams = .y)) |>
    list_rbind()

  .concepts_id <-
  concepts_root |>
    set_names() |> 
    map(~ data_match_init |>
          distinct(pick(id, group), concept) |>
          pivot_wider(names_from = concept,
                      values_from = concept) |>
          filter(if_any(matches(.), ~ !is.na(.))) |>
          select(id, group))
  
  if (concepts_intersect) {

    .match_id <- .concepts_id |> reduce(inner_join, by = c(id, group))

  } else .match_id <- data_match_init[id]

  data_match <-
  data_match_init |>
    rename(concept_key = concept) |> 
    mutate(concept = concept_key |> map_chr(~ concepts_names[[.]]),
           .after = concept_key) |>
    filter(get(id) %in% .match_id[[id]])

  data_match_df <- data |> filter(get(id) %in% data_match[[id]])

  if (nrow(data_match) == 0) {

    abort_intersect <- if (concepts_intersect) " à l'intersection" else ""

    cli_abort("{.strong Aucune correspondance{abort_intersect}}")

  }

### EXCLUSIONS -----------------------------------------------------------------

  if (!is.null(exclus_auto_except)) {

    data_match <- filter(data_match, !str_detect(get(text_input), exclus_auto_except))

  }

  set_exclus_auto <- \(regex, name) {

    data_match[[text_input]] |>
      unique() |>
      map(~ data_match |>
            filter(str_detect(get(text_input), glue(regex))) |>
            mutate(!!name := .)) |>
      list_rbind()

  }

  data_match_exclus <-
  list(auto =
         list(start = "^{.}\\s",
              end = "\\s{.}$",
              start_end = "{.}.+{.}$") |>
         imap(set_exclus_auto) |>
         reduce(full_join, by = names(data_match)) |>
         filter(ngrams > 2),
       manual =
         data_match |>
           filter(str_detect(get(text_input), exclus_manual %||% NA_character_))) |>
    imap(~ .x |>
           mutate(mode = .y, .before = everything()))

  .match_exclus <-
  list(concept =
         data_match_exclus |>
           list_rbind() |>
           pull(text_input) |>
           unique(),
       expr = expr(get(text_input) %in% .match_exclus$concept))

  data_match_final <-
  list(keep = data_match |> filter(!eval(.match_exclus$expr)),
       drop = data_match |> filter(eval(.match_exclus$expr)))

  data_count <-
  data_match_final |>
    map(~ list2(match = .,
                !!id := distinct(., pick(-group)),
                !!group := distinct(., pick(-id))) |>
          imap(~ . |>
                 count(ngrams, concept, pick(text_input),
                       name = .y,
                       sort = TRUE)) |>
          reduce(left_join, by = c("concept", text_input, "ngrams")))

  data_count_exclus <-
  data_match_exclus |>
    map(~ . |> select(-id, -group)) |>
    list_rbind() |>
    distinct() |>
    relocate(ngrams, .before = concept) |>
    left_join(data_count$drop,
              by = c("ngrams", "concept", text_input))

  data_id <- data_match_final$keep
  data_count <- data_count$keep

### REGEX ----------------------------------------------------------------------

  regex_replace <-
  c("e(?!$)" = "[eéèë]",
    "(?<=o)i" = "[iïî]",
    "\\s" = "(<br/>)?\\\\s?(-|')?\\\\s?(<br/>)?") |>
    append(regex_replace)

  regex_wrap <- "(?i)\\b({x})\\b"

  regex_replace_df <-
  tibble(pattern = names(as.list(regex_replace)),
         replace = regex_replace)

  data_regex_replace <-
  data_count |>
    arrange(concept) |>
    mutate(!!text_input :=
             str_replace_all(get(text_input), regex_replace))

  data_regex_str <-
  glue(regex_wrap,
       x =
         data_regex_replace |>
           pull(text_input) |>
           paste(collapse = "|"))

  data_regex_df <-
  data_regex_replace |>
    nest(text = text, .by = concept) |>
    mutate(!!text_input :=
             map_chr(get(text_input), ~ str_flatten(unlist(.), "|")),
           !!text_input := glue(regex_wrap, x = get(text_input)))

  data_regex_list <-
  data_regex_df$text |>
    as.list() |>
    set_names(data_regex_df$concept)

  data_regex_match <-
  data_regex_list |>
    imap(~ data_match_df |>
           edstr_view(text_input = text_input,
                      str = .x,
                      id = id,
                      quiet = TRUE) |>
           mutate(concept = .y,
                  .before = match)) |>
    list_rbind()

  data_regex_count <- 
  data_regex_match |> 
    count(concept, match, sort = TRUE)

### EXTRACTION -----------------------------------------------------------------

  data_extract <-
  list(data =
         data_match_df |>
           mutate(extract_all =
                    get(text_input) |>
                      str_extract_all(data_regex_str) |>
                      map_chr(paste, collapse = " ; ")),
       concept_name =
         data_regex_list |>
           imap(~ data_match_df |>
                  mutate(extract_all = str_extract(get(text_input), .x),
                         concept = .y) |>
                  drop_na(extract_all) |>
                  select(id, group, concept)) |>
           list_rbind() |>
           nest(concept = concept) |>
           mutate(concept = map_chr(concept, ~ str_flatten(unlist(.), " ; "))),
       concept_dummy =
         data_id |>
           distinct(pick(id, group), concept_key) |>
           pivot_wider(names_from = concept_key,
                       values_from = concept_key) |>
           mutate(across(matches(concepts_root), ~ ifelse(is.na(.), 0, 1))),
       extract_unique =
         data_id |>
           distinct(pick(id, group, text_input)) |>
           nest(extract_unique = text_input) |>
           mutate(extract_unique =
                    map_chr(extract_unique, ~ str_flatten(unlist(.), " ; ")))) |>
    reduce(inner_join, by = c(id, group)) |>
    relocate(concept, extract_all, .before = extract_unique) |>
    rownames_to_column("n")

### MISMATCH -------------------------------------------------------------------

  data_id_mismatch_base <- data[c(id, group)]

  if (mismatch_data) {

    data_id_mismatch <-
    data_id_mismatch_base |>
      filter(!get(id) %in% data_match_init[[id]])

  } else {

    data_id_mismatch <-
    data_id_mismatch_base |>
      filter(get(id) %in% NA_character_)

  }

  data_regex_match_conv <-
  data_regex_match |>
    mutate(match =
             match |>
               iconv(from = "UTF-8", to = "ASCII//TRANSLIT") |>
               tolower() |>
               str_replace_all(c("-(<br/>)?|-?<br/>" = " ",
                                 "\\s+" = " ")))

  data_mismatch <-
  list(id = data_id_mismatch,
       regex =
         data_match |>
           select(id, concept, match = !!text_input) |>
           anti_join(y = data_regex_match_conv,
                     by = c(id, "concept", "match")))

### SUMMARY --------------------------------------------------------------------

  set_summary <- \(var) {

    list(total = data_match,
         exclus_auto = data_match_exclus$auto,
         exclus_manual = data_match_exclus$manual,
         final = data_id,
         distinct = data_count) |>
      set_names(~ glue("match_{.}")) |>
      imap(~ .x |>
             count(pick(all_of(var)),
                   sort = TRUE,
                   name = .y))

  }

  data_summary <-
  list(ngrams = set_summary("ngrams"),
       concept =
         list(match = set_summary("concept"),
              id =
                list(id, group) |>
                  imap(~ summarise(data_id,
                                   !!. := n_distinct(get(.)),
                                   .by = concept))) |>
           list_flatten()) |>
    imap(~ reduce(., left_join, by = .y))

### XLSX OUTPUT ----------------------------------------------------------------

  if (xlsx_save) {

    wb_add_custom <- \(x,
                       sheet,
                       ...,
                       data,
                       width = "auto",
                       halign = "center",
                       font_size = 8,
                       font_color = "#222222",
                       concept_var = "concept",
                       concept_color = NULL,
                       text_var = with(config, text),
                       text_color = NULL,
                       border_color = "#999999",
                       border_type = "thin") {

      if (!exists(".config_name")) {

        config <- cli_error_config()

      } else config <- get(.config_name)

      .xlsx_output <-
      x |>
        wb_add_worksheet(sheet = sheet,
                         zoom = 115,
                         ...) |>
        wb_add_data_table(x = data,
                          na.strings = NULL) |>
        wb_add_font(dims = wb_dims(x = data, select = "col_names"),
                    size = font_size + 1,
                    bold = TRUE) |>
        wb_add_font(dims = wb_dims(x = data, select = "data"),
                    size = font_size) |>
        wb_add_fill(dims = wb_dims(x = data, select = "col_names"),
                    color = wb_color("grey90")) |>
        wb_set_col_widths(cols = 1:ncol(data), widths = width) |>
        wb_add_cell_style(dims = wb_dims(x = data),
                          horizontal = halign,
                          vertical = "center",
                          wrap_text = TRUE) |>
        wb_add_border(dims = wb_dims(x = data),
                      top_color = wb_color(border_color),
                      top_border = border_type,
                      bottom_color = wb_color(border_color),
                      bottom_border = border_type,
                      left_color = wb_color(border_color),
                      left_border = border_type,
                      right_color = wb_color(border_color),
                      right_border = border_type,
                      inner_hcolor = wb_color(border_color),
                      inner_hgrid = border_type,
                      inner_vcolor = wb_color(border_color),
                      inner_vgrid = border_type)

      .add_font <- \(wb, vars, color) {

        wb_add_font(wb = wb,
                    dims =
                      wb_dims(x = data,
                              cols = vars,
                              select = "data"),
                    color = wb_color(color),
                    size = font_size,
                    bold = TRUE)

      }

      if (!is.null(concept_color)) {

        .xlsx_output <-
        .add_font(.xlsx_output,
                  concept_var,
                  concept_color)

      }

      if (!is.null(text_color)) {

        .xlsx_output <-
        .add_font(.xlsx_output,
                  text_var,
                  text_color)

      }

      return(.xlsx_output)

    }

    .xlsx_mismatch <-
      lst(data = data_mismatch$regex,
          rows = if (nrow(data) > 0) data else add_row(data),
          visible = if (nrow(data) > 0) "true" else "false")

    xlsx_output <-
    wb_workbook() |>
      wb_add_custom(sheet = if (concepts_intersect) "data ∩" else "data",
                    data = data_extract |> select(-text_input, -extract_all),
                    concept_var = c(data_id$concept_key, "concept"),
                    concept_color = concept_color,
                    text_var = "extract_unique",
                    text_color = text_color) |>
      wb_add_custom(sheet = "regex_concepts",
                    data = concepts_df,
                    concept_var = names(concepts_df) |> str_subset("concept"),
                    concept_color = concept_color,
                    halign = "left") |>
      wb_add_custom(sheet = "match_concepts",
                    data = data_summary$concept,
                    concept_color = concept_color) |>
      wb_add_custom(sheet = "match_text",
                    data = data_id |> select(-concept_key),
                    concept_color = concept_color,
                    text_color = text_color) |>
      wb_add_custom(sheet = "match_count",
                    data = data_count |> select(-ngrams),
                    concept_color = concept_color,
                    text_color = text_color) |>
      wb_add_custom(sheet = "regex_replace",
                    data = regex_replace_df,
                    halign = "left") |>
      wb_add_custom(sheet = "regex_final",
                    data = data_regex_df,
                    concept_color = concept_color,
                    halign = "left") |>
      wb_add_custom(sheet = "regex_match",
                    data = data_regex_match,
                    concept_color = concept_color,
                    text_var = "match",
                    text_color = text_color) |>
      wb_add_custom(sheet = "regex_mismatch",
                    data = .xlsx_mismatch$rows,
                    concept_color = concept_color,
                    text_var = "match",
                    text_color = text_color,
                    visible = .xlsx_mismatch$visible) |>
      wb_add_custom(sheet = "regex_count",
                    data = data_regex_count,
                    concept_color = concept_color,
                    text_var = "match",
                    text_color = text_color)

  }

### HTML OUTPUT ----------------------------------------------------------------

  .css <-
  css(color = text_color,
      background.color = text_background,
      font.weight = "bold",
      font.family = "system-ui",
      padding = "0.2rem",
      border.radius = "5px")

  data_output <-
  data_extract |>
    mutate(!!text_input :=
             get(text_input) |>
               str_replace_all(glue("(?={data_regex_str})"),
                               glue("<span style='{.css}'>")) |>
               str_replace_all(glue("(?<={data_regex_str})"),
                               "</span>")) |>
    mutate(by_pat = glue("({row_number()}/{max(row_number())})"),
           by_pat = if_else(by_pat == "(1/1)", "", by_pat),
           .by = group, .before = group) |>
    select(-extract_unique)

### SAVE DATA ------------------------------------------------------------------

  data_save <-
  list(regex =
         list(concepts = concepts_df,
              replace = regex_replace_df,
              final = data_regex_df,
              match = data_regex_match),
       match =
         list(init = data_match,
              final = data_match_final),
       count =
         list(init = data_ngrams_match,
              final = data_count),
       exclus =
         list(match = data_match_exclus,
              count = data_count_exclus),
       mismatch = data_mismatch,
       summary = data_summary,
       data = data_extract,
       output = data_output)
