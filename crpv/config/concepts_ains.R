.concepts_ains <-
list(ains =
       list(ains = c(AINS = "ains\\b|anti?\\s?i?nfla?m"),
            ibu = c(ibuprofène = "ib[iuo]+pr|n[a-z]+rofe?n|advil|antare?n|sp[a-z]+fe?n"),
            ket = c(kétoprofène = "ke?topr|(b|pr)[a-z]+fe?nid"),
            nap = c(naproxène = "napro?x"),
            fnc = c(fénac = "(ac|di)[a-z]+fe?nac|voltar"),
            cxb = c(coxib = "c?e[a-z]+coxib"),
            xcm = c(xicam = "(m|p)[a-z]+xicam")),
     patho =
       list(inf =
              list(abc = c(abcès = "abces"),
                   emp = c(empyème = "e[nm]pye?m"),
                   cel = c(cellulite = "ce?l+ul+it+e"),
                   agn = c(angine = "angine|pha?r[iy]ngi?t|lemier"),
                   orl = c(orl = "otite|si[nu]+si?t|(ma?st|eth?m)[oi]+dit"),
                   cnj = c(conjonctivite = "c[on]*j[a-z]+vit"),
                   mng = c(méningite = "(me[ni]+g(o\\s?e[a-z]+)?|epidur)it"),
                   pnm = c(pneumopathie = "(pl[eu]*ro\\s?)?pn[eu]*mo?(nie|pa)"),
                   fsc = c(fasciite = "fasc[ei]+t"),
                   dhb = c(dermohypodermite = "d[er]*mo\\s?hyp[oer]*d[er]*mit|dhbnn?|er[eiy]*s[eiy]*pe?l"),
                   vrc = c(varicelle = "var+ice?l"),
                   sps = c(`sepsis/choc` = "sepsis|choc\\s(sept|tox)|de?fa[a-z]+\\s?mu?lt")),
            bact =
              list(psd = c(pseudomonas = "ps[eu]*do?mo?n"),
                   sta = c(staphylocoque = "sta?ph(\\b|[a-z]+co?[cq]+u)"),
                   str = c(streptocoque = "stre?pto(\\b|co?[cq]+u)|pyoge?n"),
                   pnm = c(pneumocoque = "pn[eu]*mo?co?q"),
                   mng = c(méningocoque = "me[ni]+[cg]o(\\b|co?q)"))))
     
save(.concepts_ains, file = "~/R/config/concepts_ains.RData")
