library(edstr)

edstr_config(dest_dir = "",
             dest_filename = "",
             str = "str.RData")

edstr_import(query = "",
             load = FALSE)

edstr_clean(load = FALSE)

edstr_extract(sample = 100,
              seed = 0,
              ngrams = 1,
              concepts = list(),
              id = "id_entrepot",
              group = "ipp")

### ----------------------------------------------------------------------------

edstr_view(data = "",
           str = "",
           id = "id_entrepot")
