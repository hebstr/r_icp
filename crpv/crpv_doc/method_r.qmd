# Extraction de texte

L'ensemble des opérations de data management utiles à l'extraction des termes recherchés est réalisé avec le langage R version 4.4

## Package {edstr}

Un package codé est R nommé **edstr** (contraction de EDS et "str", en réference au traitement de chaînes de caractères) a été créé pour répondre à 3 besoins : 

  - réalisation d'un process de collecte, de nettoyage et d'extraction du texte systématisé et reproductible
  - automatisation du reporting des règles d'extraction et des données extraites 
  - automatisation de la création et du chargement des fichiers avec des noms prédéfinis dans des répertoires prédéfinis, évitant les doublons et écrasements involontaires

documentation [ici](https://codeberg.org/hebstr/edstr) (en cours de rédaction)

organisation des fichiers et dossiers en détaillée en @sec-stockage-data

## Workflow

Ensemble du process dans un script R :

::: {add-from=K:/data/HOME/edjulien/CRPV/ains/ains_total.R code-line-numbers="true" code-filename="ains_total.R"}
```r
```
:::

workflow en 3 étapes, chacune réalisée par une fonction dédiée :

  - edstr_import : collecte de d'une table depuis EDS
  - edstr_clean : nettoyage/formatage du texte issu de la table collectée (en plus de conversion des espaces pendant extraction features)
  - edstr_extract : extraction et reporting automatisé 
  
Chaque fonction permet la création de nouveaux fichiers mais aussi le chargement de fichiers existants afin d'octroyer un gain de temps et court-circuiter une ou plusieurs étapes. 
Le process est facilité par la prédéfinition des noms de fichiers et des répertoires de destination.
  
edstr_view() : fonction accessoire pour exploration rapide du texte par recherche de mots-clés, avec extraction et dénombrement, sans création de fichiers externes
permet de tester simplement la pertinence les règles de nettoyage et d'extraction du texte

Se référer directement à la documentation du package pour le fonctionnement détaillé de chaque fonction

## Règles d'extraction

recherche de concepts à partir de 3 axes :

   - Dénomination des AINS (DCI et princeps)
   - Dénomination des infections bactériennes
   - Dénomination des agents pathogènes

lexique selon dernier rapport ANSM [lien](https://ansm.sante.fr/uploads/2024/04/30/20240130-cr-csp-psubuexbu.pdf)

L'objectif étant de détecter les documents faisant à la fois mention d'une prise d'AINS et d'une complication infectieuse, on regroupe ces 3 catégories de concepts en 2 grands axes :

  - Axe "ains" : les concepts AINS, chacun regroupant DCI et princeps pour une molécule donnée (e.g. concept "ibuprofène" comprenant toutes spécialités à base d'ibuprofène, génériques et princeps)
  - Axe "patho" : les infections bactériennes et agents pathogènes

on pourra ainsi extraire les documents d'intérêt = ceux situés à l'intersection ains ET patho

liste de concepts détaillée ci-dessous :

::: {add-from=K:/data/HOME/edjulien/R/config/concepts_ains.R code-line-numbers="true" code-filename="concepts_ains.R"}
```r
```
:::

## Reporting automatisé

Les règles d'extraction et les données issues de l'extraction du texte sont rapportées de façon systématisée dans un tableur au format .xlsx

une information par onglet :
  
  1) table des caractéristiques de base + variables extraction, filtrée sur les documents avec correspondances à l'intersection
  2) recherche tokens : détail des règles d'extraction utilisées pour chaque concept
  3) recherche tokens : dénombrement des concepts, une ligne par concept
  4) recherche tokens : détail des correspondances, une ligne par correspondance
  5) recherche tokens : dénombrement des correspondances, une ligne par correspondance distincte
  6) recherche texte : motifs de remplacement pour les règles d'extractions finales
  7) recherche texte : détail des règles d'extraction utilisées pour chaque concept
  8) recherche texte : mismatch entre recherche par token et par le texte, une ligne par correspondance
  10) recherche texte : dénombrement des correspondances, une ligne par correspondance distincte

