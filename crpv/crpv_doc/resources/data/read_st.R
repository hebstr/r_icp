tbl_st_index <-
tbl_xlsx$total$`data ∩` |> 
  mutate(texte = "<texte>") |>
  select(n, id_entrepot, id_pat, ipp, id_sej, iep, age, sexe, 
         date_entree, date_sortie, code_uf, libelle_uf,
         titre_doc, texte, concept, extract_unique) |> 
  filter(n == 28) |> #295 
  gt_qmd(font_size = 10) |> 
  text_align() |> 
  text_color(column = "concept",
             color = .theme$concept) |> 
  text_color(column = "extract_unique",
             color = .theme$text)
