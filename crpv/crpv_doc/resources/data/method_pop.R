tbl_uf <-
tbl_flow |>
  map(~ . |>
        set_names("total", "union", "inter") |> 
        imap(~ .x |> 
               count(code_uf_doc, libelle_uf_doc,
                     name = .y,
                     sort = TRUE)) |>
        reduce(inner_join,
               by = c("code_uf_doc", "libelle_uf_doc")))

tbl_uf_group <- \(.data) {
    
  uf |>
    imap(~ tbl_uf[[.data]] |>
           filter(str_starts(code_uf_doc, .x)) |>
           mutate(uf_group = .y)) |> 
    list_rbind() |> 
    select(code_uf_doc, uf_group) |> 
    full_join(tbl_uf[[.data]], by = "code_uf_doc") |> 
    mutate(uf_group = str_replace_na(uf_group, "autre"))

}

tbl_uf_group_out <-
.year |> 
map(~ tbl_uf_group(.) |> 
      reactable(defaultSorted = list(inter = "desc"),
                groupBy = "uf_group",
                defaultExpanded = TRUE,
                defaultPageSize = 100,
                showSortable = TRUE,
                filterable = TRUE,
                striped = TRUE,
                resizable = TRUE,
                defaultColDef = colDef(align = "center"),
                columns =
                  list(uf_group = colDef(minWidth = 120),
                       code_uf_doc = colDef(style = list(fontWeight = "bold")),
                       libelle_uf_doc = 
                         colDef(minWidth = 200,
                                align = "left",
                                style = list(fontWeight = "bold"))),
                theme = 
                  reactableTheme(style = 
                                   list(fontSize = "0.7em",
                                        fontFamily = "luciole"),
                                 stripedColor = "#E0F9FF")))

n_total_match <- tbl_uf |> map(~ sum(.$inter))

tbl_uf_total <-
.year |> 
  map(~ tbl_uf_group(.) |>
        summarise(n = sum(inter),
                  p = label_percent(0.1)(n / n_total_match[[.]]),
                  .by = uf_group))

n_uf_total <-
.year |> 
  map(~ lst(data =
              tbl_uf_total[[.]] |>
                filter(uf_group %in% names(uf)) |>
                pull(n),
            str = paste(data, collapse = " + "),
            n = sum(data),
            p = label_percent(0.1)(n / n_total_match[[.]]),
            np = glue("{n} ({p})")))
    