# Reporting

{{< include _setup.qmd >}}

données de janvier 2018 : résultats dans `S:/_CRPV_/ains/ains_2018_01_extract.xlsx`

organisation des fichiers et dossiers détaillée en @sec-stockage-data

## Onglet data

Une seule table, ici divisée en 2 parties pour plus de clarté
  
  - 1e partie : caractéristiques par document = table extraite depuis EDS
  - 2e partie : jointure des résultats extraction

::: panel-tabset

# data : Caractéristiques de base

caractéristiques sans variable texte

1 ligne par document

ne peut pas contenir plusieurs fois un même id_entrepot

```{r}
tbl_xlsx$total$`data ∩` |> 
  select(1:type_doc) |> 
  reactable(defaultPageSize = 10,
            showSortable = TRUE,
            striped = TRUE,
            defaultColDef = colDef(align = "center", vAlign = "center"),
            theme = .theme$rt)
```

# Résultats de l'extraction

2e partie de la table : détail des correspondances retrouvées dans le document

variables binaires :

Une pour pour chaque concept

  - valeur 0 : absent du texte
  - valeur 1 : présent au moins une fois dans le texte
  
Ne sont présents que les concepts ayant au moins une valeur 1 parmi l'ensemble des documents
  
concept : ensemble des concepts dont la correspondance associé est présente au moins une fois dans le document, séparés par un point-virgule

extract_all : ensemble des correspondances présentes dans le texte, tous concepts confondus, séparées par un point-virgule 

```{r}
tbl_gt_xlsx$data
```

:::

## Concepts

::: panel-tabset

# regex_concepts

Pour chaque concept, détail des expressions recherchées dans le texte splité

```{r}
tbl_gt_xlsx$regex_concepts
```

concept_key : nom "brut", identifiant du concept servant de clé pour des opérations de data management (intersection et/ou regroupements)

concept_name : libellé, plus informatif, qui sera utilisé dans les tables suivantes

regex : expression 

# match_concepts

Décomptes des concepts

```{r}
tbl_gt_xlsx$match_concepts
```

:::

## Texte

::: panel-tabset

# match_text

1 ligne par correspondance 

```{r}
tbl_gt_xlsx$match_text
```

# match_count

Décompte des correspondances

```{r}
tbl_gt_xlsx$match_count
```

:::

## Regex

::: panel-tabset

# regex_replace

```{r}
tbl_gt_xlsx$regex_replace
```

# regex_final

```{r}
tbl_gt_xlsx$regex_final
```

# regex_match

```{r}
tbl_gt_xlsx$regex_match
```

# regex_mismatch

```{r}
tbl_gt_xlsx$regex_mismatch
```

# regex_count

```{r}
tbl_gt_xlsx$regex_count
```

:::



