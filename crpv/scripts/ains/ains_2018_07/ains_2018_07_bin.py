import pandas as pd
import streamlit as st
import os
from streamlit.components.v1 import html
#from pwd_function import check_password

#if not check_password("secrets_crpv.toml") : st.stop() 

### INPUT ----------------------------------------------------------------------

wd = f"{os.getcwd()}/CRPV" 

filename = "ains"
date = "2018_07" 
file_date = f"{filename}_{date}"
extract = f"{file_date}_extract"
dirname = f"{wd}/{filename}/{file_date}/data/{extract}"
path = f"{dirname}/{extract}"

title = f"{file_date.upper()}"
text = "text"

data = pd.read_csv(f"{path}.csv", dtype = str)

### OUTPUT ---------------------------------------------------------------------

output = f"{path}_output.csv"
 
if os.path.exists(output): ref = pd.read_csv(output)
else :
    ref = data.drop([text], axis = 1)
    ref.loc[:, "ains_bin"] = "-"
    ref.loc[:, "comment"] = "-"
    ref.to_csv(output, index = False)

ref["by_pat"] = ref["by_pat"].fillna("")

### SESSION --------------------------------------------------------------------

st.set_page_config(
    page_title = title,
    layout = "wide"
) 

ss = st.session_state

def i(var) :
    val = ref[var].iloc[ss.doc_index]
    return val

max_index = ref[ref["ains_bin"] != "-"].index.max()

if "doc_index" not in ss :
    ss.doc_index = 0 if pd.isna(max_index) else max_index

ss.ains_bin = i("ains_bin")
ss.comment = i("comment")

### STYLE ----------------------------------------------------------------------

st.markdown(
    f"""
    <style>
    {open("./R/config/streamlit.css").read()}
    </style>
    """,
    unsafe_allow_html = True
)

### DOC ------------------------------------------------------------------------

### DOC > HEADER ---------------------------------------------------------------

col_id_1, col_id_2, col_id_3 = st.columns([1.2, 2.2, 1.5])

with col_id_1 :
    st.write(
        f"""
        **ID DOC** : {i("id_entrepot")}\\
        **ID SEJ** : {i("id_sej")} -- **ID PAT** : {i("id_pat")}\\
        **IEP** : {i("iep")} -- **IPP** : {i("ipp")} {i("by_pat")}\\
        **AGE** : {i("age")} -- **SEXE** : {i("sexe")}
        """
    )

with col_id_2 :
    st.write(
        f"""
        **DATE ENTRÉE** : {i("date_entree")}\\
        **UF ENTREE** : {i("code_uf_entree")} - {i("libelle_uf_entree")}\\
        **UF DOC** : {i("code_uf_doc")} - {i("libelle_uf_doc")}\\
        **TITRE DOC** : {i("titre")}
        """
    )

with col_id_3 :
    st.html(
        f"""
        <p style='margin-bottom:0.3rem;'>Correspondances :</p>
        <p class='highlight'>{i("extract_text")}</p>
        """
    )

### DOC > CONTENT --------------------------------------------------------------

html(
    list(data[text])[ss.doc_index],
    height = 800,
    scrolling = True
)

### SIDEBAR --------------------------------------------------------------------

bin_radio = {"Oui": 0, "Non": 1, " ": None}
bin_chr = ["Oui", "Non"]

def reset():
    ss.ainsbin = (
        None
        if i("ains_bin") not in bin_chr
        else i("ains_bin")
    )
    ss.keycomment = i("comment")

### SIDEBAR > HEADER -----------------------------------------------------------

st.sidebar.title(title)

col_sh_1, col_sh_doc, col_sh_n, col_sh_total, col_sh_5 = (
    st.sidebar.columns([0.8, 1, 1.3, 1, 0.8])
)

def number_change() :
    ss.doc_index = ss.num - 1
    reset()

with col_sh_doc :
    st.html(
        "<p class='doc-number' style='text-align:right'>Document</p>"
    )

with col_sh_n :
    st.number_input(
        label = "Document",
        label_visibility = "collapsed",
        value = i("n"),
        min_value = 1,
        max_value = len(ref),
        step = 1,
        key = "num",
        on_change = number_change
    )

with col_sh_total : 
    st.html(
        f"<p class='doc-number' style='text-align:left'>sur {len(ref)}</p>"
    )

st.sidebar.divider()

### SIDEBAR > FEATURES ---------------------------------------------------------

ss.ains_bin = st.sidebar.radio(
    label = "Document retenu",
    options = ("Oui", "Non"),
    index = (
        None
        if ss.ains_bin not in bin_chr
        else bin_radio[ss.ains_bin]
    ),
    key = "ainsbin"
)

ss.comment = st.sidebar.text_input(
    label = "Commentaires",
    value = ss.comment,
    key = "keycomment"
)

st.sidebar.divider()

### SIDEBAR > NAV --------------------------------------------------------------

def loc() :
    ref.loc[ss.doc_index, "ains_bin"] = ss.ains_bin
    ref.loc[ss.doc_index, "comment"] = ss.comment

col_prev, col_save, col_next = st.sidebar.columns([1, 2, 1])

def update_precedent() :
    loc()
    if ss.doc_index > 0 : ss.doc_index -= 1
    reset()

with col_prev :
    st.button(
        label = ":material/arrow_back:",
        help = "Document précédent",
        use_container_width = True, 
        on_click = update_precedent,
    )

def update_suivant() :
    loc()
    if ss.doc_index < len(ref) - 1 : ss.doc_index += 1
    reset()

with col_next :
    st.button(
        label = ":material/arrow_forward:",
        help = "Document suivant",
        use_container_width = True,
        on_click = update_suivant,
    )

def update_save() :
    loc()
    ref.to_csv(output, index = False)

with col_save :
    st.button(
        label = "**Enregistrer**",
        use_container_width = True,
        on_click = update_save,
    )
