--create table ains_2023_07 as 
select eh.id_entrepot, 
       eh.id_sej, esm.iep, eh.id_pat, epm.ipp, ep.datenais date_nais,
       coalesce(round((es.date_entree - ep.datenais) / 365.25, 1), eh.age_pat) age, ep.sexe,
       es.date_entree, es.date_sortie, round(es.date_sortie - es.date_entree, 1) duree_sej,
       to_date(es.date_sortie) - to_date(es.date_entree) nb_nuit,
       de.date_deces_chu, de.date_deces_insee,
       es.uf_entree code_uf_entree, str_e.libelle_uf_entree,
       eh.uf code_uf_doc, str_d.libelle_uf_doc,
       eh.titre, eh.type_doc, regexp_replace(eh.texte_affichage, '(&#160;)+', ' ') text
from(
     select *
     from edbm_eds.ehop_entrepot
     where not regexp_like(type_doc, 'RSS|RUM|LN:|HISTORIQUE|ORDSORT')
       and not regexp_like(titre, 'rapport d''intervention|check list', 'i')
       and regexp_like(uf, '^(30[8-9]|14[0-3]|2(2([1-2]|30)|30)|5(29|30)|42(3|5))')
    ) eh
inner join(
           select *
           from edbm_eds.ehop_sejour es
           where date_entree between '01/07/23' and '01/08/23'
           order by id_pat, date_entree
          ) es
        on eh.id_sej = es.id_sej
inner join(
           select *
           from edbm_eds.ehop_texte
           where certitude = '1'
             and rang_contexte = '1'
             and contexte = 'texte'
             --and contains(texte, q'[]') > 0
          ) et
        on eh.id_entrepot = et.id_entrepot
inner join(
           select *
           from edbm_zpat.ehop_patient_mapping epm
           where retrait = '0'
             and not exists(
                            select ipp
                            from noyau_eds.patients_opposition op
                            where epm.ipp = op.ipp
                           )
          ) epm
       on eh.id_pat = epm.id_pat
left join(
          select exercice, code_uf, libelle_uf libelle_uf_entree
          from structure_chu_historique
         ) str_e
       on extract(year from es.date_entree) = str_e.exercice
      and es.uf_entree = str_e.code_uf
left join(
          select exercice, code_uf, libelle_uf libelle_uf_doc
          from structure_chu_historique
         ) str_d
       on extract(year from es.date_entree) = str_d.exercice
      and eh.uf = str_d.code_uf
left join edbm_zpat.ehop_sejour_mapping esm
       on eh.id_sej = esm.id_sej
left join edbm_eds.ehop_patient ep
       on eh.id_pat = ep.id_pat
left join data_eds.deces_eds de
       on eh.id_pat = de.id_pat
