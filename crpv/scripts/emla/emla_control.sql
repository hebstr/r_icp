select count(eh.id_entrepot) id_entrepot,
       count(distinct(eh.id_sej)) id_sej,
       count(distinct(eh.id_pat)) id_pat
from(
     select *
     from edbm_eds.ehop_entrepot
     where not regexp_like(type_doc, 'RSS|RUM|LN:|HISTORIQUE|ORDSORT')
       and not regexp_like(titre, 'rapport d''intervention|check list', 'i')
       and age_pat <= 15
    ) eh
inner join(
           select *
           from edbm_eds.ehop_texte
           where certitude = '1'
             and rang_contexte = '1'
             and contexte = 'texte'
             and contains(texte, q'[m%t%ogl%b% OR m%t%hb%]') > 0
          ) et
        on eh.id_entrepot = et.id_entrepot
inner join(
           select *
           from edbm_zpat.ehop_patient_mapping epm
           where retrait = '0'
             and not exists(
                            select ipp
                            from noyau_eds.patients_opposition op
                            where epm.ipp = op.ipp
                           )
          ) epm
       on eh.id_pat = epm.id_pat
left join(
           select *
           from edbm_eds.ehop_sejour
           --where date_entree between '01/01/16' and '01/01/25'
           ) es
        on eh.id_sej = es.id_sej
