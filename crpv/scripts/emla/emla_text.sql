select id_entrepot, id_sej, iep, id_pat, ipp,
       datenais date_nais, age_pat age, sexe,
       annee_entree, annee_sortie, date_entree, date_sortie,
       round(date_sortie - date_entree, 1) duree_sej,
       to_date(date_sortie) - to_date(date_entree) nb_nuit,
       date_deces_chu, date_deces_insee,
       to_date(date_deces_chu) - date_deces_insee deces_diff,
       to_date(date_deces_chu) - to_date(date_sortie) sortie_deces_chu_diff,
       to_date(date_deces_insee) - to_date(date_sortie) sortie_deces_insee_diff,
       code_uf_sejour,
       uf_entree code_uf_entree, libelle_uf_entree,
       uf code_uf_doc, libelle_uf_doc,
       titre, type_doc,
       regexp_replace(texte_affichage, '(&#160;)+|(_|-){10,}', ' ') text
from(
     select eh.id_entrepot, eh.id_pat, eh.id_sej, eh.uf, eh.age_pat,
            eh.titre, eh.type_doc, eh.texte_affichage,
            epm.ipp,
            es.date_entree, es.date_sortie, es.annee_entree, es.annee_sortie, es.uf_entree,
            de.date_deces_insee, de.date_deces_chu,
            str_e.libelle_uf_entree,
            str_d.libelle_uf_doc,
            uf.code_uf_sejour,
            esm.iep,
            ep.sexe, ep.datenais
     from(
          select *
          from edbm_eds.ehop_entrepot
          where not regexp_like(type_doc, 'RSS|RUM|LN:|HISTORIQUE|ORDSORT')
            and not regexp_like(titre, 'rapport d''intervention|check list', 'i')
            and age_pat <= 15
         ) eh
     inner join(
                select *
                from edbm_eds.ehop_texte
                where certitude = '1'
                  and rang_contexte = '1'
                  and contexte = 'texte'
                  and contains(texte, q'[m%t%ogl%b% OR m%t%hb%]') > 0
               ) et
             on eh.id_entrepot = et.id_entrepot
     inner join(
                select *
                from edbm_zpat.ehop_patient_mapping epm
                where retrait = '0'
                  and not exists(
                                 select ipp
                                 from noyau_eds.patients_opposition op
                                 where epm.ipp = op.ipp
                                )
               ) epm
            on eh.id_pat = epm.id_pat
     left join(
               select es.*,
                      extract(year from date_entree) annee_entree,
                      extract(year from date_sortie) annee_sortie
               from edbm_eds.ehop_sejour es
               --where date_entree between '01/01/16' and '01/01/24'
               order by id_pat, date_entree
              ) es
            on eh.id_sej = es.id_sej
     left join(
               select exercice, code_uf, libelle_uf libelle_uf_entree
               from structure_chu_historique
              ) str_e
            on es.annee_entree = str_e.exercice
           and es.uf_entree = str_e.code_uf
     left join(
               select exercice, code_uf, libelle_uf libelle_uf_doc
               from structure_chu_historique
              ) str_d
            on es.annee_entree = str_d.exercice
           and eh.uf = str_d.code_uf
     left join(
               select id_sej,
                      listagg(uf, ';') within group (order by date_entree) code_uf_sejour
               from edbm_eds.ehop_sejour_uf
               group by id_sej
              ) uf
            on eh.id_sej = uf.id_sej
     left join edbm_zpat.ehop_sejour_mapping esm
            on eh.id_sej = esm.id_sej
     left join edbm_eds.ehop_patient ep
            on eh.id_pat = ep.id_pat
     left join data_eds.deces_eds de
            on eh.id_pat = de.id_pat
    )
