#######ANALYSE1CODE######
#obj de tester toute la bio pr?sence oui/non ->all, bio r?sultat (qti ou quali) mean/min/max/nb ->ceux qui l'ont, administration de m?dicaments ->all, codes CIM ->all

source("G:/Data/gregoire.ficheur/database_connection/connectDatabase.R")

library(data.table)
library(Matrix)

#chargement des s?jours hospitaliers
tab_realisations_pmsi<-dbGetQuery(con, "SELECT DISTINCT id_pat, id_sej, DATE_DATA FROM edbm_eds.EHOP_ENTREPOT_STRUCTURE where CODE_THESAURUS='PMSI'")
tab_realisations_pmsi$DATE_DATA<-as.Date(tab_realisations_pmsi$DATE_DATA)

#crit?re 2017-6/2021
tab_realisations_pmsi<-tab_realisations_pmsi[tab_realisations_pmsi$DATE_DATA>="2021-01-01" & tab_realisations_pmsi$DATE_DATA<"2021-07-01",]
tab_realisations_pmsi<-tab_realisations_pmsi[order(tab_realisations_pmsi$DATE_DATA),]
tab_realisations_pmsi_un_iep<-tab_realisations_pmsi[!duplicated(tab_realisations_pmsi[,c("ID_PAT")]),]


#on ajoute ?ge et sexe
tab_age_sexe<-dbGetQuery(con, "SELECT ID_PAT, DATENAIS, SEXE FROM edbm_eds.EHOP_PATIENT")
tab_age_sexe$DATENAIS<-as.Date(tab_age_sexe$DATENAIS)
tab_age_sexe<-tab_age_sexe[!tab_age_sexe$SEXE %in% c("I","U"),]
print(nrow(tab_realisations_pmsi_un_iep))
tab_realisations_pmsi_un_iep<-merge(tab_realisations_pmsi_un_iep, tab_age_sexe, by="ID_PAT")
print(nrow(tab_realisations_pmsi_un_iep))

#crit?re d'?ge
tab_realisations_pmsi_un_iep$age<-round(as.numeric((tab_realisations_pmsi_un_iep$DATE_DATA-tab_realisations_pmsi_un_iep$DATENAIS)/365.25),1)
tab_realisations_pmsi_un_iep<-tab_realisations_pmsi_un_iep[tab_realisations_pmsi_un_iep$age>=50,]
print(nrow(tab_realisations_pmsi_un_iep))

tab_inclus<-tab_realisations_pmsi_un_iep


#chargement des codes cim
tab_codes_cim10<-dbGetQuery(con, "SELECT ID_SEJ, CODE FROM edbm_eds.EHOP_ENTREPOT_STRUCTURE where CODE_THESAURUS='cim10'")
lignes_a_corriger<-regexpr(" ", tab_codes_cim10$CODE)!=-1
tab_codes_cim10[lignes_a_corriger,"CODE"]<-substr(tab_codes_cim10$CODE[lignes_a_corriger], 1, regexpr(" ", tab_codes_cim10$CODE)[lignes_a_corriger]-1)
tab_codes_cim10<-unique(tab_codes_cim10)

tab_inclus$fesf<-0
mes_id_sej_fesf<-unique(tab_codes_cim10[regexpr("S7200|S7210",tab_codes_cim10$CODE)!=-1,"ID_SEJ"])
print(length(mes_id_sej_fesf))
tab_inclus$fesf[tab_inclus$ID_SEJ %in% mes_id_sej_fesf] <- 1

write.csv2(tab_inclus, file = 'tab_inclus_test.csv')

