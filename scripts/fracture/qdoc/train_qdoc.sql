create table train_qdoc as
select id_entrepot, t.*, titre,
       regexp_replace(
           regexp_replace(
               regexp_replace(
                   regexp_replace(
                       regexp_replace(texte_affichage,
                       '\<!-- Page \d+ --\>\s\<a name=\"\d+\"\>\</a\>\s', ''),
                   '\<!--\s+p {.+}|\<(/|!\w+\s)?(html|head|title|meta|div|style).*\>', ''),
               ' (style|class|id|bgcolor|v?link)=.+\"|.ft\d+{.+}', ''),
           '\.x?y?flip.+(\s+.+){6}|\<!--|--\>|\</?(i|b)\>|\s{3}|\<p\> \</p\>', ''),
       '\s*(\[\w+_?\w+\]|&#160;|(_|-){10,})\s*', ' ') text
from w_etudes.table_inclus t
join(
     select *
     from edbm_eds.ehop_entrepot
     where id_entrepot in(
                         select id_entrepot
                         from(
                             select  'SIDOSMED_' || doc_c_num as nouv_col
                             from sill_doc@sillage_dg_sipsdm_i
                             where MOD_C_REF = 'QDOCCR'
                             ) a
                         left join edbm_zdoc.ehop_doc_source b
                           on a.nouv_col = b.id_doc_source
                          and id_entrepot is not null
                         )
     ) ee
  on t.id_sej = ee.id_sej