create table avc_pmsi_ls as
select id_entrepot, id_sej, iep, id_pat, ipp,
       datenais date_nais, age_pat age, sexe,
       date_entree, date_sortie,
       date_deces_chu, date_deces_insee,
       to_date(date_deces_chu) - date_deces_insee deces_diff,
       to_date(date_deces_chu) - to_date(date_sortie) sortie_deces_chu_diff,
       to_date(date_deces_insee) - to_date(date_sortie) sortie_deces_insee_diff,
       annee_entree, annee_sortie, annee_sortie - annee_entree nb_annee,
       to_date(date_sortie) - to_date(date_entree) nb_nuit,
       round(date_sortie - date_entree, 1) as duree_sej,
       uf_entree code_uf_entree, libelle_uf libelle_uf_entree, 
       code_dp, titre, type_doc,
       regexp_replace(
           regexp_replace(
               regexp_replace(
                   regexp_replace(
                       regexp_replace(texte_affichage,
                       '\<!-- Page \d+ --\>\s\<a name=\"\d+\"\>\</a\>\s', ''),
                   '\<!--\s+p {.+}|\<(/|!\w+\s)?(html|head|title|meta|div|style).*\>', ''),
               ' (style|class|id|bgcolor|v?link)=.+\"|.ft\d+{.+}', ''),
           '\.x?y?flip.+(\s+.+){6}|\<!--|--\>|\</?(i|b)\>|\s{3}|\<p\> \</p\>', ''),
       '\s*(\[\w+_?\w+\]|&#160;|(_|-){10,})\s*', ' ') text
from(
    select eh.id_entrepot, eh.id_pat, eh.id_sej, eh.uf, eh.age_pat, 
           eh.titre, eh.type_doc, eh.texte_affichage,
           epm.ipp,
           es.date_entree, es.date_sortie, es.annee_entree, es.annee_sortie,
           es.uf_entree,
           ees.code_dp,
           de.date_deces_insee, de.date_deces_chu,
           uf.libelle_uf,
           esm.iep,
           ep.sexe, ep.datenais
    from(
        select *
        from edbm_eds.ehop_entrepot
        where not regexp_like(type_doc, 'RSS|RUM|LN:|HISTO|ORD|IMAG|CERTIF|VIDE|DI|AUTRE')
          and regexp_like(titre, 'sortie|liaison', 'i')
          and age_pat >= 18
        ) eh
    join(
        select *
        from edbm_eds.ehop_texte
        where certitude = '1'
          and rang_contexte = '1'
          and contexte = 'texte'
        ) et
      on eh.id_entrepot = et.id_entrepot
    join(
        select *
        from edbm_zpat.ehop_patient_mapping
        where retrait = '0'
        ) epm
      on eh.id_pat = epm.id_pat
    join( 
        select es.*,
               extract(year from date_entree) annee_entree,
               extract(year from date_sortie) annee_sortie
        from edbm_eds.ehop_sejour es
        where date_entree between '01/01/22' and '01/01/23'
        ) es
      on eh.id_sej = es.id_sej
    join(
        select distinct id_sej, code code_dp
        from edbm_eds.ehop_entrepot_structure
        where texte = 'Principal'
          and regexp_like(code, 'I6(3|5)')
        ) ees
      on eh.id_sej = ees.id_sej
    join structure_chu_historique uf
      on es.annee_entree = uf.exercice
     and es.uf_entree = uf.code_uf
    join edbm_zpat.ehop_sejour_mapping esm
      on eh.id_sej = esm.id_sej
    join edbm_eds.ehop_patient ep
      on eh.id_pat = ep.id_pat
    left join data_eds.deces_eds de
           on eh.id_pat = de.id_pat
    )