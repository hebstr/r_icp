create table sevrage_text as
select eh.id_entrepot, eh.id_sej, eh.id_pat, eh.ipp, eh.iep, ee.age_pat, eh.sexe,
       eh.uf, eh.uf_entree, eh.date_entree, eh.date_sortie, eh.date_deces,
       ee.titre, ee.type_doc,
       regexp_replace(
           regexp_replace(
               regexp_replace(
                   regexp_replace(
                       regexp_replace(texte_affichage,
                       '\<!-- Page \d+ --\>\s\<a name=\"\d+\"\>\</a\>\s', ''),
                   '\<!--\s+p {.+}|\<(/|!\w+\s)?(html|head|title|meta|div|style).*\>', ''),
               ' (style|class|id|bgcolor|v?link)=.+\"|.ft\d+{.+}', ''),
           '\.x?y?flip.+(\s+.+){6}|\<!--|--\>|\</?(i|b)\>|\s{3}|\<p\> \</p\>', ''),
       '\s*(\[\w+_?\w+\]|&#160;|(_|-){10,})\s*', ' ') text
from(
    select eh.id_entrepot, eh.uf,
           et.id_pat, et.id_sej,
           es.date_entree, es.date_sortie, es.uf_entree,
           epm.ipp, epm.retrait,
           esm.iep,
           ep.sexe, ep.date_deces
    from(
        select id_entrepot, uf
        from edbm_eds.ehop_entrepot
        where regexp_like(titre, 'Synth.se des r.al')
        ) eh
    join(
        select id_entrepot, id_pat, id_sej
        from edbm_eds.ehop_texte
        where certitude = '1'
          and rang_contexte = '1'
          and contexte = 'texte'
          and contains(texte,
          q'[sevrag% AND (alc% OR ethyl% OR oh OR benzo% OR bzd)
             OR
             sevrag% AND (alc% OR ethyl% OR oh OR benzo% OR bzd)
                     AND (dt OR gw OR cc OR cris% convul% OR cris% comit%
                          OR hal%uc% OR tremul% OR tr.mbl% OR agita% OR agite%)
             OR
             del%r%m OR gayet OR korsa%]') > 0
        ) et
      on eh.id_entrepot = et.id_entrepot
    join edbm_zpat.ehop_patient_mapping epm
      on et.id_pat = epm.id_pat
    join edbm_zpat.ehop_sejour_mapping esm
      on et.id_sej = esm.id_sej
    join edbm_eds.ehop_patient ep
      on et.id_pat = ep.id_pat
    join edbm_eds.ehop_sejour es
      on et.id_sej = es.id_sej
    where epm.retrait = '0'
      and es.date_entree between '01/01/22' and '01/01/23'
    ) eh
join edbm_eds.ehop_entrepot ee
  on eh.id_entrepot = ee.id_entrepot
where age_pat >= 15