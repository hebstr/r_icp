library(gt)
source("init.R")
edstr_config(data = "config.RData",
             path = "../data/sevrage/f102_ggr")

load("../data/sevrage/match/sevrage_tsm.RData")
iep_vp <- "../data/sevrage/iep_vp.csv"

txt_import(query = "select id_entrepot, id_sej, id_pat, iep, 
                           code_f, diag_f, code_g, diag_g, 
                           date_entree, date_sortie, duree_sej,
                           uf_entree, uf_libelle
                    from sevrage_f102_ggr",
           dir = "../database_connection",
           user = "w_etudes",
           load = TRUE)

data_f102_ggr <-
lst(cim =
      f102_ggr_import |>
        filter(diag_f %in% c("Principal", "Associe", "Relie"),
               diag_g %in% c("Principal", "Associe", "Relie")) |>
        mutate(diag_f = factor(diag_f),
               diag_g = factor(diag_g),
               code_f = code_f |> str_extract(".{4}") |> factor(),
               code_g = code_g |> str_extract(".{4}") |> factor()),
    text =
      cim |>
        filter(id_entrepot %in% sevrage_tsm$str$brut$id_entrepot),
    text_vp =
      cim |>
        filter(iep %in% read_csv(iep_vp)$iep))

data_f102_ggr |>
  map(~ . |>
        distinct(id_sej, code_f, code_g) |>
        filter(!duplicated(id_sej)) |>
        count(code_f, code_g)) |>
  reduce(left_join, by = join_by(code_f, code_g)) |>
  rename(cim = n.x,
         text = n.y,
         text_vp = n) |> 
  gt(rowname_col = "code") |>
  sub_missing(missing_text = 0) |> 
  tab_style(style = cell_text(align = "center",
                              weight = "bold"),
            locations = list(cells_column_labels(),
                             cells_stub())) |> 
  tab_style(style = cell_text(align = "center"),
            locations = list(cells_body()))

data_merge <-
data_f10$cim |> 
  rename(code_f = code) |> 
  filter(!duplicated(id_sej),
         str_detect(code_f, "F10(6|7)")) |> 
  bind_rows(data_f102_ggr$cim |> filter(!duplicated(id_sej)))

data_merge |>
  mutate(across(starts_with("date"), as_date)) |> 
  relocate(code_g, .after = code_f) |> 
  select(-c(id_entrepot, id_sej, id_pat, starts_with("diag"))) |> 
  writexl::write_xlsx(path = "../data/sevrage/temp_fg.xlsx")
